#Green Papaya Salad - Som Tum ส้มตำ

2## Servings, Prep Time: 10 Minutes, Total Time: 10 Minutes

5 Cherry Tomatoes
2 chili peppers
1 tablespoon dried shrimp
1 1/2 tablespoons fish sauce
1 clove garlic
6 green beans
2 cups shredded green papaya
3/4 lime
1 1/2 tablespoons Palm Sugar
2 tablespoons toasted peanuts Optional

###Tips and Techniques
For a vegetarian som tum, omit the dried shrimp and substitute soy sauce for fish sauce.
Some people use tamarind in place of lime. Regular sugar can be substituted for palm sugar. I normally omit the peanuts because I prefer it without.
The balance of fish sauce, lime juice, palm sugar and peppers listed here are guidelines. Som tum is an individual dish that you will find you might like yours with more, say, lime juice than what the recipe calls for.

Many Asian supermarkets have pre-shredded green papaya and that is what I use. However, if you can only find whole green papaya, the papaya can be peeled and shredded using a regular cheese grater with medium to large sized holes. Or if you can find a papaya shredder, it works wonder. When you get closer to the center, you will see the white immature seeds inside. Stop and move onto another part of the papaya. Discard any seeds that got into your bowl. If you have a food processor with grater, you can shred the papaya faster.

In Thailand, green papaya salad is made using a clay mortar, wooden pestle and a spatula. Smash a clove of garlic first. Then add green beans and halved cherry tomatoes. Pound a few times just to bruise the beans and get the juice out of the tomatoes. Add chili peppers and crush them just enough to release the hotness, unless you like your salad really hot. Add the green papaya, dried shrimp, toasted peanuts, fish sauce, lime juice and palm sugar. Use the pestle to push the mixture up in the mortar and the spatula to push it down so that the mixture is mixed well.

However, if you do not have a big enough mortar you can crush garlic, tomatoes, green beans. Set them aside in a large bowl. Add dried shrimp, fish sauce, lime juice and palm sugar to the bowl. Add green papaya and mix well.

Serve with sticky rice and a sliver of cabbage, green beans and Thai basil.

For som tum pbooh, omit the dried shrimp and toasted peanuts and add salted crabs instead. I microwave the salted crabs for 30 seconds before adding them to the papaya salad to kill any residual bacteria. Add only half of the fish sauce called for because the salted crab can be quite salty.